/*
 * This file is part of Aix.
 * 
 * Aix is free software: you can redistribute it and/or modify
 * it under the terms of the Affero GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Aix is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Affero GNU General Public License for more details.
 * 
 * You should have received a copy of the Affero GNU General Public License
 * along with Aix.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * I kind of fucking despise this file.
 */

#define MAX_TOKS	256

#ifndef __OpenBSD__
#include <bsd/string.h>
#include <bsd/stdlib.h>
#include <linux/limits.h>
#endif

#include <limits.h>

#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include <err.h>

#include "parser.h"

#define HEADER_LEN		1
#define SUBHEADER_LEN	2

/* Source helper functions. */
static const char		*_parser_get_line(char *, const char *, size_t);
static struct root_node	*_parser_handle_line(struct root_node *, const char *);

/* Inline parser helper function. */
static struct inline_node	*_parser_handle_inline(const char *);
static struct inline_node	*_parser_create_child(enum inline_style, const char *);

/* Paragraph line parsers. */
static struct root_node	*_parser_p_new();

/* Header line parsers. */
static struct root_node	*_parser_h_new(enum root_style, const char *);

/* Pre line parsers. */
static struct root_node	*_parser_c_new();

/* Blockquote line parser. */
static struct root_node	*_parser_q_new(const char *);

/* Link and image line parser. */
static struct root_node *_parser_a_new(enum root_style, const char *);

/* List line parser. */
static struct root_node *_parser_l_new(enum root_style, const char *);

/* Special and inline helper functions. */
#define	_parser_inline_add(r,l)	_parser_inline_add_new(r, l, 1)
static void				 _parser_inline_add_new(struct root_node *, const char *, int);
static struct root_node	*_parser_create_node(enum root_style);
static struct root_node	*_parser_e_new();

struct root_node *
parser_read_file(FILE *fp)
{
	struct root_node	*root;
	struct root_node	*cur;

	const char			*line;
	char				*src_data;
	size_t			 	 src_data_len;

	if (NULL == fp)
		err(1, "parser cannot read NULL file");

	root = calloc(sizeof(struct root_node), 1);

	/* Read in the source file. */
	fseek(fp, 0, SEEK_END);
	src_data_len = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	src_data = malloc(sizeof(char) * src_data_len + 1);
	fread(src_data, sizeof(char), src_data_len, fp);

	/* Setup the final null terminator for the file.
	 * This is used by _parser_get_line */
	src_data[src_data_len] = '\0';

	/* TODO: Maybe don't do this? This is here in order to avoid dangling
	 * a file pointer around while the actual data is parsed. */
	fclose(fp);

	/* Iterate over the given source file, and convert each line into
	 * the appropriate linked list node. For paragraphs, this will adjust
	 * the inner nodes of the current node, and return the current node
	 * again. */
	cur = root;
	line = NULL;
	while (NULL != (line = _parser_get_line(src_data, line, src_data_len)))
		cur = _parser_handle_line(cur, line);

	free(src_data);

	/* Return the head of the linked list. */
	return root;
}

void
parser_free(struct root_node *root)
{
	struct root_node	*next_root;
	struct inline_node	*child;
	struct inline_node	*next_child;

	do {
		child = root->children;

		while (NULL != child) {
			if (NULL != child->text)
				free(child->text);

			next_child = child->next;
			free(child);

			child = next_child;
		}

		if (NULL != root->meta)
			free(root->meta);

		next_root = root->next;
		free(root);

		root = next_root;
	} while (NULL != root);
}

static const char *
_parser_get_line(char *src, const char *last_line, size_t src_len)
{
	size_t	line_off;
	size_t	i;

	/* If last_line is NULL, we start at the beginning, otherwise we
	 * calculate the offset based on the last line's offset to the beginning
	 * of the source data, and then add the lines length, including null
	 * terminator. */
	if (NULL == last_line)
		line_off = 0;
	else
		line_off = (last_line - src) + (strlen(last_line) + 1);

	/* Check for EOF. */
	if (line_off >= src_len)
		return NULL;

	/* Skip until next newline. */
	for (i = line_off; i < src_len && src[i] != '\n'; i++);

	/* Set the null terminator for the new line, unless it's the last line. */
	if (i < src_len)
		src[i] = '\0';

	return (const char *)(src + line_off);
}

/*
 * This function is, frankly, quite a mess.
 * TODO: Work on cleaning up this function.
 * TODO: Convert ascii comparisons to use valid UTF-8 comparisons.
 */
static struct root_node *
_parser_handle_line(struct root_node *node, const char *line)
{
	struct root_node	*new;
	size_t 				 line_len;
	size_t				 line_start;

	line_len = strlen(line);
	new = NULL;

	/* If we have a blank line, create a new paragraph node. */
	if (0 == line_len) {
		new = _parser_p_new();

		node->next = new;
		return new;
	}

	/* If we're in a pre-block, skip the normal parser. */
	if (RSTYLE_PRE == node->style) {
		/* This code sure isn't clean, but oh well. */
		if (strstr(line, "---") == line) {
			new = _parser_e_new();
			node->next = new;

			return new;
		} else {
			_parser_inline_add_new(node, line, 0);
			return node;
		}
	}

	/* Find the start of the actual line, skipping spaces. */
	/* for (line_start = 0; line[line_start] != ' '; line_start++); */
	line_start = 0; /* For an initial implementation, don't skip whitespace. */

	/* Look at the first character of the line to infer functionality of
	 * this line. */
	switch (line[line_start]) {
	case '>':	/* Header or subheader. */
		if (line[line_start + 1] == '>')
			new = _parser_h_new(RSTYLE_SUBHEADER, line + line_start);
		else
			new = _parser_h_new(RSTYLE_HEADER, line + line_start);
		break;
	case '\t':	/* Blockquote. */
		if (RSTYLE_QUOTE == node->style)
			_parser_inline_add(node, line + line_start + 1);
		else
			new = _parser_q_new(line + line_start + 1);

		break;
	case '&':	/* Possibly a link or image. */
		if (line[line_start + 1] == 'a') {
			new = _parser_a_new(RSTYLE_LINK, line + line_start);
			break;
		}
		else if (line[line_start + 1] == 'i') {
			new = _parser_a_new(RSTYLE_IMAGE, line + line_start);
			break;
		}

		/* Otherwise fall through. */
	case '.':	/* Possibly an unordered list. */
		if (strstr(line, "..") == line + line_start) {
			new = _parser_l_new(RSTYLE_LIST_UNORDER, line + line_start);
			break;
		}

		/* Fall through to the pre case, which will fall through to the
		 * default parsing case. */
	case '-':	/* Possibly a pre-block. */
		if (strstr(line, "---") == line + line_start) {
			new = _parser_c_new();
			break;
		}

		/* Otherwise we fall through into the default line parser. */
	default:
		/* First check if it's an ordered list line. */
		if (isdigit(line[line_start]) && line[line_start + 1] == '.')
			new = _parser_l_new(RSTYLE_LIST_ORDER, line + line_start);
		else {
			/* We're dealing with a paragraph line. */
			if (RSTYLE_PARAGRAPH != node->style)
				new = _parser_p_new();
			else
				new = node;

			_parser_inline_add(new, line + line_start);
		}
		break;
	}

	if (NULL == new)
		return node;

	/* Append the next node, if it's actually new. */
	if (node != new)
		node->next = new;

	return new;
}

static struct root_node *
_parser_create_node(enum root_style s)
{
	struct root_node	*new;
	
	new = calloc(sizeof(struct root_node), 1);
	new->style = s;

	return new;
}

static struct inline_node *
_parser_create_child(enum inline_style s, const char *text)
{
	struct inline_node *new;
	size_t				text_len;
	
	new = calloc(sizeof(struct inline_node), 1);
	new->style = s;
	
	text_len = strlen(text) + 1;
	new->text = malloc(sizeof(char) * text_len);
	strlcpy(new->text, text, text_len);

	return new;
}

static struct root_node *
_parser_p_new()
{
	return _parser_create_node(RSTYLE_PARAGRAPH);
}

static struct root_node *
_parser_c_new()
{
	return _parser_create_node(RSTYLE_PRE);
}


static struct root_node *
_parser_e_new()
{
	return _parser_create_node(RSTYLE_END);
}

static struct root_node *
_parser_q_new(const char *line)
{
	struct root_node	*q;
	q = _parser_create_node(RSTYLE_QUOTE);
	_parser_inline_add(q, line);

	return q;
}

static struct root_node *
_parser_h_new(enum root_style s, const char *line)
{
	struct root_node	*h;

	h = _parser_create_node(s);

	/* Skip any whitespace in the line after the header syntax. */
	while(*line == '>') line++;
	while(isspace(*line)) line++;

	switch (s) {
	case RSTYLE_HEADER:
		h->children = _parser_create_child(ISTYLE_NORMAL, line);
		break;
	case RSTYLE_SUBHEADER:
		h->children = _parser_create_child(ISTYLE_NORMAL, line);
		break;
	default:
		errx(1, "invalid header style given");
	}

	return h;
}

static struct root_node *
_parser_l_new(enum root_style s, const char *line)
{
	struct root_node	*l;

	l = _parser_create_node(s);

	/* XXX: Bad hardcoded badness. */
	l->children = _parser_handle_inline(line + 2);

	return l;
}

static struct root_node *
_parser_a_new(enum root_style s, const char *line)
{
	const char			*meta;
	size_t				 meta_len;
	struct root_node 	*a;

	a = _parser_create_node(s);

	/* Skip until after the syntax tag. */
	for (; !isspace(*line); line++);
	line++; /* Skip the space as well. */

	meta = line;

	/* Skip until after the link/meta. */
	for (; !isspace(*line) && *line != '\0'; line++);

	meta_len = line - meta + 1;

	/* And then skip any final whitespace. */
	for (; isspace(*line) && *line != '\0'; line++);

	/* If the line isn't over, set the alt/label. */
	if (*line != '\0') {
		if (RSTYLE_LINK == s)
			a->children = _parser_handle_inline(line);
		else
			a->children = _parser_create_child(ISTYLE_NORMAL, line);
	}
	else
		line += 1; /* Hack to ensure null terminator is included below. */

	/* Set the meta on the node itself. */
	a->meta = malloc(sizeof(char) * meta_len);
	strlcpy(a->meta, meta, meta_len);

	return a;
}

static struct inline_node *
_parser_handle_inline(const char *line)
{
	struct inline_node	 *root;
	struct inline_node	 *node;
	const char			**toks;
	char				 *buffer;
	size_t				  tok_i;
	size_t				  line_len;
	size_t				  i;
	size_t				  j;

	line_len = strlen(line) + 1;
	buffer = malloc(sizeof(char) * line_len);

	toks = malloc(sizeof(char *) * MAX_TOKS);
	tok_i = 0;

	/* Hard set 0 as a "normal" token. */
	toks[tok_i++] = line;

	/* First gather the position of all tokens. */
	for (i = 0; i < line_len; i++) {
		switch (line[i]) {
		case '*':
		case '_':
		case '|':
			toks[tok_i++] = line + i;
			break;
		}
	}

	/* Add the final tok, the end of line. */
	toks[tok_i++] = line + line_len;

	/* Handle the root node. */
	strlcpy(buffer, toks[0], (toks[1] - toks[0]) + 1);
	root = _parser_create_child(ISTYLE_NORMAL, buffer);

	/* Iterate over all tokens to create nodes.
	 * Off we goooooooo. */
	node = root;
	for (i = 1; i < tok_i - 1; i++) {
		for (j = i + 1; j < tok_i; j++) {
			if (*toks[j] == *toks[i]) {
				/* We have a matching token, setup the node. */
				strlcpy(buffer, (toks[i] + 1), toks[j] - toks[i]);

				switch (*toks[i]) {
				case '*':
					node->next = _parser_create_child(ISTYLE_EM, buffer);
					break;
				case '_':
					node->next = _parser_create_child(ISTYLE_UNDERLINE, buffer);
					break;
				case '|':
					node->next = _parser_create_child(ISTYLE_STRONG, buffer);
					break;
				default:
					warn("unknown inline style token: %c\n", *toks[i]);
				}

				/* Skip to the next tok outside of this block. */
				node = node->next;
				i = j;
				break;
			}
		}

		/* Add the normal space between tokens. */
		strlcpy(buffer, (toks[i] + 1), toks[i + 1] - toks[i]);
		node->next = _parser_create_child(ISTYLE_NORMAL, buffer);
		node = node->next;
	}

	free(buffer);
	return root;
}

static void
_parser_inline_add_new(struct root_node *node, const char *line, int parse)
{
	struct inline_node *last;

	if (NULL == node->children)
		node->children = _parser_handle_inline(line);
	else {
		/* Find the last inline node. */
		for (last = node->children; last->next != NULL; last = last->next);

		if (parse)
			last->next = _parser_handle_inline(line);
		else
			last->next = _parser_create_child(ISTYLE_NORMAL, line);
	}
}
