/*
 * This file is part of Aix.
 * 
 * Aix is free software: you can redistribute it and/or modify
 * it under the terms of the Affero GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Aix is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Affero GNU General Public License for more details.
 * 
 * You should have received a copy of the Affero GNU General Public License
 * along with Aix.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sys/stat.h>

#include <unistd.h>
#include <getopt.h>

#ifndef __OpenBSD__
#include <bsd/string.h>
#include <bsd/stdlib.h>
#include <linux/limits.h>
#endif

#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <stdio.h>
#include <errno.h>

#include <libgen.h>
#include <err.h>

#include "config.h"
#include "parser.h"
#include "gemtext.h"
#include "html.h"
#include "raw.h"

static char			 cfg_file[PATH_MAX] = "aix.conf";
static struct cfg	*cfg;
static enum mode	 mode;

static void	usage(void);

int
main(int argc, char *argv[])
{
	struct root_node	*bytecode;
	const char			*errstr;
	FILE				*fp;
	char				*bn;
	int 				 opt;
	int					 i;

#ifdef __OpenBSD__
	if (-1 == pledge("stdio rpath wpath cpath unveil", NULL))
		err(1, "pledge failed");
#endif

	mode = MODE_UNKNOWN;
	errstr = NULL;

	while (-1 != (opt = getopt(argc, argv, "hm:f:"))) {
		switch (opt) {
		case 'm':
			mode = strtonum(optarg, 0, MAX_MODE, &errstr);
			if (NULL != errstr)
				errx(1, "bad mode: %s", errstr);
			break;
		case 'f':
			strlcpy(cfg_file, optarg, PATH_MAX);
			break;
		case 'h':
		default:
			usage();
			return 0;
		}
	}

#ifdef __OpenBSD__
	if (-1 == unveil(cfg_file, "rwc"))
		err(1, "config file unveil failed");
#endif

	if (optind >= argc)
		errx(1, "no input files");

	fp = fopen(cfg_file, "r");
	if (NULL == fp)
		err(1, "%s", cfg_file);

	/* Load the config, which unveils required files and folders. */
	cfg = cfg_from_file(fp);
	if (NULL == cfg)
		errx(1, "failed to load config file");

	fclose(fp);

	/* Create the output directory. */
	if (-1 == mkdir(cfg->output_dir, 0755) && EEXIST != errno)
		err(1, "unable to create output directory");

#ifdef __OpenBSD__
	/* Re unveil the output directory, as now it's created. */
	unveil(cfg->output_dir, "wc");
#endif

	if (MODE_UNKNOWN == mode)
		mode = cfg->output_mode;

#ifdef __OpenBSD__
	/* Unveil the source files. */
	for (i = optind; i < argc; i++) {
		if (-1 == unveil(argv[i], "r"))
			err(1, "unveil failed for \"%s\"", argv[i]);
	}

	/* Disable future unveils. */
	unveil(NULL, NULL);
#endif

	/* Then parse and render the output. */
	for (i = optind; i < argc; i++) {
		fp = fopen(argv[i], "r");
		if (NULL == fp) {
			warn("%s", argv[i]);
			continue;
		}

		/* Parsing the source file will also close the file pointer
		 * automatically. */
		bytecode = parser_read_file(fp);
		if (NULL == bytecode) {
			warnx("%s: failed to parse", argv[i]);
			continue;
		}

		bn = basename(argv[i]);

		switch (mode) {
		case MODE_RAW:
			raw_to_stdout(bn, bytecode);
			break;
		case MODE_HTML:
			html_from_bytecode(bn, cfg, bytecode);
			break;
		case MODE_GMI:
			gmi_from_bytecode(bn, cfg, bytecode);
			break;
		default:
			errx(1, "no mode set");
		}

		parser_free(bytecode);
	}

	cfg_free(cfg);
	return 0;
}

static void
usage(void)
{
	puts("aix: version " VERSION ", licensed AGPLv3\n"
		"usage:\n"
		"\taix [-h] [-m MODE] [-f <cfg_file>] ... [FILES]\n");
}
