/*
 * This file is part of Aix.
 * 
 * Aix is free software: you can redistribute it and/or modify
 * it under the terms of the Affero GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Aix is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Affero GNU General Public License for more details.
 * 
 * You should have received a copy of the Affero GNU General Public License
 * along with Aix.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef AIX_UTIL_H
#define AIX_UTIL_H

#include "config.h"

FILE	*util_get_fp(struct cfg *, char *, const char *);
void	 util_output_included(FILE *out, char *included_fn);

#endif
