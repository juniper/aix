/*
 * This file is part of Aix.
 * 
 * Aix is free software: you can redistribute it and/or modify
 * it under the terms of the Affero GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Aix is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Affero GNU General Public License for more details.
 * 
 * You should have received a copy of the Affero GNU General Public License
 * along with Aix.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>

#include "raw.h"

/* TODO: Formalize this into a spec. */

void
raw_to_stdout(char *fn, struct root_node *root)
{
	struct inline_node	*child;

	/* We must first output the file next w/ the new file indicator. */
	printf("FN %s\n", fn);

	/* Iterate over the linked lists and output the data to stdout. */
	for (; NULL != root; root = root->next) {
		printf("%c %s\n", root->style, NULL == root->meta ? "" : root->meta);
		
		for (child = root->children; NULL != child; child = child->next)
			printf("\t%c %s\n", child->style, child->text);
	}
}
