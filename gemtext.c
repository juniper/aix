/*
 * This file is part of Aix.
 * 
 * Aix is free software: you can redistribute it and/or modify
 * it under the terms of the Affero GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Aix is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Affero GNU General Public License for more details.
 * 
 * You should have received a copy of the Affero GNU General Public License
 * along with Aix.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>

#include "gemtext.h"
#include "util.h"

static void	_gmi_render_doc(FILE *out, struct cfg *, struct root_node *);
static void	_gmi_render_children(FILE *out, struct inline_node *, int);

void
gmi_from_bytecode(char *bn, struct cfg *cfg, struct root_node *root)
{
	FILE	*out;

	out = util_get_fp(cfg, bn, ".gmi");
	if (NULL == out)
		return;

	if (NULL != cfg->gmi_header)
		util_output_included(out, cfg->gmi_header);

	_gmi_render_doc(out, cfg, root);

	if (NULL != cfg->gmi_footer)
		util_output_included(out, cfg->gmi_footer);

	if (stdout != out)
		fclose(out);
}

static void
_gmi_render_doc(FILE *out, struct cfg *cfg, struct root_node *root)
{
	struct root_node	*node;
	int					 add_nl;

	for (node = root; node != NULL; node = node->next) {
		add_nl = 1;

		switch (node->style) {
		case RSTYLE_HEADER:
			if (cfg->gmi_convert_headers) fputs("## ", out);
			else fputs("# ", out);

			_gmi_render_children(out, node->children, 0);
			break;
		case RSTYLE_SUBHEADER:
			if (cfg->gmi_convert_headers) fputs("### ", out);
			else fputs("## ", out);

			_gmi_render_children(out, node->children, 0);
			break;
		case RSTYLE_LIST_ORDER:
		case RSTYLE_LIST_UNORDER:
			fputs("* ", out);
			_gmi_render_children(out, node->children, 0);
			break;
		case RSTYLE_PARAGRAPH:
			_gmi_render_children(out, node->children, 0);

			/* If we have paragraphs next to one another, and they have text
			 * within them, ensure a separating newline is present. */
			if (NULL != node->next
					&& RSTYLE_PARAGRAPH == node->next->style
					&& NULL != node->next->children)
				fputs("\n", out);
			break;
		case RSTYLE_QUOTE:
			fputs("> ", out);
			_gmi_render_children(out, node->children, 0);
			break;
		case RSTYLE_PRE:
			fputs("```\n", out);
			_gmi_render_children(out, node->children, 1);
			fputs("\n```", out);
			break;
		case RSTYLE_LINK:
		case RSTYLE_IMAGE:
			fprintf(out, "=> %s ", node->meta);
			_gmi_render_children(out, node->children, 0);
			break;
		default:
			/* Eat styles that aren't used. */
			add_nl = 0;
			break;
		}

		if (add_nl)
			fputs("\n", out);
	}
}

static void
_gmi_render_children(FILE *out, struct inline_node *children, int add_nl)
{
	struct inline_node	*child;

	for (child = children; child != NULL; child = child->next) {
		fputs(child->text, out);
		
		if (add_nl)
			fputs("\n", out);
	}
}
