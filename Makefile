include config.mk

SOURCES!=ls *.c
OBJECTS=$(SOURCES:c=o)

.PHONY: clean install uninstall

.SUFFIXES: .c .o
.c.o:
	$(CC) -c -o $@ $< $(CFLAGS)

aix: $(OBJECTS)
	$(CC) -fPIE -o $@ $(OBJECTS) $(CFLAGS) $(LIBBSD)

install: aix
	install -C -m 0755 aix			$(BIN_DIR)/aix
	install -C -m 0755 aix.1		$(MAN_DIR)/man1/aix.1
	install -C -m 0755 aix.5		$(MAN_DIR)/man5/aix.5
	install -C -m 0755 aix.conf.5	$(MAN_DIR)/man5/aix.conf.5
	@echo You may need to run \"doas makewhatis $(MAN_DIR)\".

uninstall:
	rm -f $(BIN_DIR)/aix
	rm -f $(MAN_DIR)/man1/aix.1
	rm -f $(MAN_DIR)/man5/aix.5 $(MAN_DIR)/man5/aix.conf.5

clean:
	rm *.o aix
