/*
 * This file is part of Aix.
 * 
 * Aix is free software: you can redistribute it and/or modify
 * it under the terms of the Affero GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Aix is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Affero GNU General Public License for more details.
 * 
 * You should have received a copy of the Affero GNU General Public License
 * along with Aix.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <limits.h>

#ifndef __OpenBSD__
#include <bsd/string.h>
#include <bsd/stdlib.h>
#include <linux/limits.h>
#endif

#include <string.h>
#include <stdlib.h>

#include <err.h>

#include "util.h"

FILE *
util_get_fp(struct cfg *cfg, char *bn, const char *ext)
{
	char	*path;
	char	 name[PATH_MAX];
	size_t	 path_len;
	size_t	 name_len;
	size_t	 dir_len;
	FILE	*fp;

	if (cfg->force_stdout)
		return stdout;

	dir_len = strlen(cfg->output_dir);

	for (name_len = 0; name_len < strlen(bn); name_len++) {
		if (bn[name_len] == '.')
			break;
	}

	if (name_len >= strlen(bn)) name_len--;

	/* get the name value. */
	strlcpy(name, bn, name_len + 1);
	strlcat(name, ext, PATH_MAX);

	path_len = dir_len + name_len + 7;
	path = malloc(sizeof(char) * path_len);

	strlcpy(path, cfg->output_dir, path_len);

	if (cfg->output_dir[dir_len - 1] != '/')
		strlcat(path, "/", path_len);

	strlcat(path, name, path_len);

	fp = fopen(path, "w");

	if (NULL == fp) {
		warn("%s", path);
		free(path);

		return NULL;
	}

	free(path);
	return fp;
}

void
util_output_included(FILE *out, char *included_fn)
{
	char	*buffer;
	FILE	*fp;
	size_t	 len;

	if (NULL == (fp = fopen(included_fn, "r")))
		warn("%s", included_fn);
	else {
		fseek(fp, 0, SEEK_END);
		len = ftell(fp);
		fseek(fp, 0, SEEK_SET);

		buffer = malloc(sizeof(char) * len);

		fread(buffer, sizeof(char), len, fp);
		fwrite(buffer, sizeof(char), len, out);

		free(buffer);
		fclose(fp);
	}
}
