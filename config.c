/*
 * This file is part of Aix.
 * 
 * Aix is free software: you can redistribute it and/or modify
 * it under the terms of the Affero GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Aix is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Affero GNU General Public License for more details.
 * 
 * You should have received a copy of the Affero GNU General Public License
 * along with Aix.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __OpenBSD__
#include <bsd/string.h>
#include <bsd/stdlib.h>
#include <linux/limits.h>
#endif

#include <unistd.h>
#include <getopt.h>
#include <limits.h>
#include <ctype.h>

#include <stdlib.h>
#include <string.h>

#include <err.h>

#include "config.h"

/* If people like whitespace to much this may end up causing problems,
 * hopefully not though. */
#define	PROP_MAX		128

enum line_status {
	LINE_PRE,
	LINE_PROP,
	LINE_VALUE,
	LINE_COMMENT
};

struct line {
	char prop[PROP_MAX];
	char value[PATH_MAX];
};

static char		*cfg_data;
static size_t	 cfg_data_len;

static int	_cfg_parse_line(struct cfg *);
static int	_cfg_handle_line(struct cfg *, struct line *);
static int	_cfg_set_str(char **, const char *);

struct cfg *
cfg_from_file(FILE *fp)
{
	struct cfg	*cfg;

	if (NULL == fp)
		err(1, "bad file pointer when loading config");

	cfg = calloc(sizeof(struct cfg), 1);
	cfg->output_mode = MODE_UNKNOWN;

	/* Get the file size and create a heap buffer for the data. */
	fseek(fp, 0, SEEK_END);
	cfg_data_len = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	cfg_data = malloc(sizeof(char) * cfg_data_len);

	/* Read the file into memory. */
	if (fread(cfg_data, sizeof(char), cfg_data_len, fp) != cfg_data_len)
		err(1, "error occurred reading config file");

	/* Continually parse lines until the helper detects EOF. */
	if (_cfg_parse_line(cfg))
		errx(1, "error parsing config file");

	/* Cleanup the file buffer. */
	free(cfg_data);
	cfg_data = NULL;

	return cfg;
}

void
cfg_free(struct cfg *cfg)
{
	if (NULL != cfg->output_dir)
		free(cfg->output_dir);

	if (NULL != cfg->html_header)
		free(cfg->html_header);

	if (NULL != cfg->html_footer)
		free(cfg->html_footer);

	if (NULL != cfg->html_style)
		free(cfg->html_style);

	free(cfg);
}

static int
_cfg_parse_line(struct cfg *cfg)
{
	struct line			line;
	enum line_status 	stat;
	size_t				val_start;
	size_t				i;

	stat = LINE_PRE;
	for (i = 0; i < cfg_data_len; i++) {
		/* Basic control flow handling. */
		if ('#' == cfg_data[i])
			stat = LINE_COMMENT;

		/* If we're a comment, we don't care about any of this. 
		 * TODO: Handle edge case where a comment starts at the end of a
		 * config line without whitespace (e.g. "Prop Value# comment") */
		if (LINE_COMMENT == stat) {
			if ('\n' == cfg_data[i])
				stat = LINE_PRE;

			continue;
		}

		/* Handling how to parse the current non-space data. */
		if (!isspace(cfg_data[i]) && (i < 1 || isspace(cfg_data[i - 1])))
			val_start = i;
		else if (isspace(cfg_data[i]) && (i > 0 && !isspace(cfg_data[i - 1])))
			stat++;
		else
			continue;

		switch (stat) {
		case LINE_PROP:
			/* Only load the property name if there's one actually there. */
			if (i - val_start > 0)
				strlcpy(line.prop, cfg_data + val_start, i - val_start + 1);
			break;
		case LINE_VALUE:
			/* Load the property name and handle this prop line. */
			strlcpy(line.value, cfg_data + val_start, i - val_start + 1);

			if (_cfg_handle_line(cfg, &line))
				return 1;

			break;
		default:
			break;
		}

		if ('\n' == cfg_data[i])
			stat = LINE_PRE;
	}

	return 0;
}

static int
_cfg_handle_line(struct cfg *cfg, struct line *l)
{
	const char *errstr;

	errstr = NULL;
	if (0 == strcmp(l->prop, "OutputMode")) {
		cfg->output_mode = (enum mode)strtonum(l->value, 0, MAX_MODE, &errstr);

		if (NULL != errstr) {
			warnx("bad output mode: %s", errstr);
			return 1;
		}
	}
	else if (0 == strcmp(l->prop, "OutputDirectory")) {
		if (_cfg_set_str(&cfg->output_dir, l->value))
			return 1;

#ifdef __OpenBSD__
		if (-1 == unveil(cfg->output_dir, "wc"))
			err(1, "output directory unveil failed");
#endif
	}
	else if (0 == strcmp(l->prop, "GmiHeader")) {
		if (_cfg_set_str(&cfg->gmi_header, l->value))
			return 1;

#ifdef __OpenBSD__
		if (-1 == unveil(cfg->gmi_header, "r"))
			err(1, "Gmi header file unveil failed");
#endif
	}
	else if (0 == strcmp(l->prop, "GmiFooter")) {
		if (_cfg_set_str(&cfg->gmi_footer, l->value))
			return 1;

#ifdef __OpenBSD__
		if (-1 == unveil(cfg->gmi_footer, "r"))
			err(1, "Gmi footer file unveil failed");
#endif
	}
	else if (0 == strcmp(l->prop, "GmiConvertHeaders")) {
		cfg->gmi_convert_headers = (enum mode)strtonum(l->value, 0, 1, &errstr);

		if (NULL != errstr) {
			warnx("bad boolean value: %s", errstr);
			return 1;
		}
	}
	else if (0 == strcmp(l->prop, "HTMLHeader")) {
		if (_cfg_set_str(&cfg->html_header, l->value))
			return 1;

#ifdef __OpenBSD__
		if (-1 == unveil(cfg->html_header, "r"))
			err(1, "HTML header file unveil failed");
#endif
	}
	else if (0 == strcmp(l->prop, "HTMLFooter")) {
		if (_cfg_set_str(&cfg->html_footer, l->value))
			return 1;

#ifdef __OpenBSD__
		if (-1 == unveil(cfg->html_footer, "r"))
			err(1, "HTML footer file unveil failed");
#endif
	}
	else if (0 == strcmp(l->prop, "HTMLStyle")) {
		if (_cfg_set_str(&cfg->html_style, l->value))
			return 1;
	}
	else if (0 == strcmp(l->prop, "HTMLFavicon")) {
		if (_cfg_set_str(&cfg->html_favicon, l->value))
			return 1;
	}
	else if (0 == strcmp(l->prop, "ForceStdout")) {
		cfg->force_stdout = (enum mode)strtonum(l->value, 0, 1, &errstr);

		if (NULL != errstr) {
			warnx("bad boolean value: %s", errstr);
			return 1;
		}
	}
	else {
		warnx("unknown prop \"%s\"", l->prop);
		return 1;
	}

	return 0;
}

static int
_cfg_set_str(char **dst, const char *src)
{
	*dst = malloc(sizeof(char) * strlen(src) + 1);
	if (NULL == dst) {
		warn("failed to malloc value");
		return 1;
	}

	if (strlcpy(*dst, src, strlen(src) + 1) != strlen(src)) {
		warnx("bad strlcpy");
		return 1;
	}

	return 0;
}
