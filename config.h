/*
 * This file is part of Aix.
 * 
 * Aix is free software: you can redistribute it and/or modify
 * it under the terms of the Affero GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Aix is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Affero GNU General Public License for more details.
 * 
 * You should have received a copy of the Affero GNU General Public License
 * along with Aix.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef AIX_CONFIG_H
#define AIX_CONFIG_H

#include <stdint.h>
#include <stdio.h>

#define MAX_MODE	MODE_GMI
enum mode {
	MODE_UNKNOWN	= -1,
	MODE_RAW		=  0,
	MODE_HTML		=  1,
	MODE_GMI		=  2
};

struct cfg {
	enum mode	 output_mode;
	int			 force_stdout;
	char		*output_dir;
	int			 gmi_convert_headers;
	char		*gmi_header;
	char		*gmi_footer;
	char		*html_header;
	char		*html_footer;
	char		*html_style;
	char		*html_favicon;
};

struct cfg	*cfg_from_file(FILE *);
void		 cfg_free(struct cfg *);

#endif
