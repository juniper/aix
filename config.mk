CC?=cc

# The default optimization level.
O?=2

# The default installation directories.
BIN_DIR?=/usr/local/bin
MAN_DIR?=/usr/local/man

VERSION=1.1.2

WARN_FLAGS=-Wall -Wextra -Wimplicit-fallthrough=0 -Werror
STDC_FLAGS=-std=c99 -pedantic

CFLAGS=$(WARN_FLAGS) $(STDC_FLAGS) -O$(O) -DVERSION=\"$(VERSION)\"

LIBBSD?=-lbsd
