/*
 * This file is part of Aix.
 * 
 * Aix is free software: you can redistribute it and/or modify
 * it under the terms of the Affero GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Aix is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Affero GNU General Public License for more details.
 * 
 * You should have received a copy of the Affero GNU General Public License
 * along with Aix.  If not, see <https://www.gnu.org/licenses/>.
 */

/* TODO: Possibly also support the XHTML standard.
 * This may be a separate exporter though. */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <err.h>

#include "html.h"
#include "util.h"

#define DOCTYPE "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\""\
				" \"http://www.w3.org/TR/html4/strict.dtd\">"

static void	 _html_output_head(FILE *, struct cfg *, struct root_node *);
static void	 _html_output_body(FILE *, struct cfg *, struct root_node *);
static void	 _html_output_children(FILE *, struct inline_node *, int);
static void	 _html_emit_tag(FILE *, const char *, struct inline_node *);

void
html_from_bytecode(char *bn, struct cfg *cfg, struct root_node *root)
{
	FILE	*fp;

	fp = util_get_fp(cfg, bn, ".html");
	if (NULL == fp)
		return;

	fputs(DOCTYPE "<html>", fp);

	_html_output_head(fp, cfg, root);
	_html_output_body(fp, cfg, root);

	fputs("</html>", fp);

	if (fp != stdout)
		fclose(fp);
}

static void
_html_output_head(FILE *out, struct cfg *cfg, struct root_node *root)
{
	struct inline_node	*child;
	struct root_node 	*node;

	fputs("<head>", out);

	/* Output the icon and stylesheet, if we have any. */
	if (NULL != cfg->html_style) {
		fprintf(out, "<link rel=\"stylesheet\" type=\"text/css\" href=\"%s\"/>",
			cfg->html_style);
	}

	if (NULL != cfg->html_favicon) {
		fprintf(out, "<link rel=\"icon\" type=\"image/x-icon\" href=\"%s\"/>",
			cfg->html_favicon);
	}

	/* Iterate over the structure to pull out the first header and paragraph
	 * to use as meta tags for title and description. */

	/* TODO: Make this process more efficient. */

	/* First the subject/header. */
	for (node = root; NULL != node; node = node->next) {
		if (RSTYLE_HEADER == node->style && NULL != node->children) {
			/* Output the title. */
			fprintf(out, "<title>%s</title>",
				node->children->text);

			/* Followed by the metadata. */
			fprintf(out, "<meta name=\"subject\" content=\"%s\"/>",
				node->children->text);
			break;
		}
	}

	/* Then the description/paragraph. */
	for (node = root; NULL != node; node = node->next) {
		if (RSTYLE_PARAGRAPH == node->style && NULL != node->children) {
			fprintf(out, "<meta name=\"description\" content=\"");

			/* Iterate over all children to extract text. */
			for (child = node->children; NULL != child; child = child->next)
				fprintf(out, "%s", child->text);

			fprintf(out, "\"/>");
			break;
		}
	}

	fputs("</head>", out);
}

static void
_html_output_body(FILE *out, struct cfg *cfg, struct root_node *root)
{
	struct root_node	*node;
	int					 in_list;

	fputs("<body>", out);

	if (NULL != cfg->html_header)
		util_output_included(out, cfg->html_header);

	in_list = 0;
	for (node = root; NULL != node; node = node->next) {
		switch (node->style) {
		case RSTYLE_HEADER:
			_html_emit_tag(out, "h1", node->children);
			break;
		case RSTYLE_SUBHEADER:
			_html_emit_tag(out, "h2", node->children);
			break;
		case RSTYLE_PARAGRAPH:
			/* Only care about non-empty paragraphs. */
			if (NULL != node->children)
				_html_emit_tag(out, "p", node->children);
			break;
		case RSTYLE_LIST_UNORDER:
			if (in_list != 1) {
				if (in_list == 2)
					fputs("</ol>", out);
				fputs("<ul>", out);
			}

			_html_emit_tag(out, "li", node->children);
			break;
		case RSTYLE_LIST_ORDER:
			if (in_list != 2) {
				if (in_list == 1)
					fputs("</ul>", out);
				fputs("<ol>", out);
			}

			_html_emit_tag(out, "li", node->children);
			break;
		case RSTYLE_QUOTE:
			_html_emit_tag(out, "blockquote", node->children);
			break;
		case RSTYLE_PRE:
			/* Use divs to make the pre block function as a block. */
			fputs("<div>", out);
			_html_emit_tag(out, "pre", node->children);
			fputs("</div>", out);
			break;
		/* The following require custom logic, and cannot use _html_emit_tag */
		case RSTYLE_LINK:
			fprintf(out, "<div><a href=\"%s\">", node->meta);

			if (NULL != node->children)
				_html_output_children(out, node->children, 0);
			else
				fputs(node->meta, out);

			fputs("</a></div>", out);
			break;
		case RSTYLE_IMAGE:
			fprintf(out, "<img src=\"%s\" ", node->meta);

			if (NULL != node->children)
				fprintf(out, "alt=\"%s\"", node->children->text);

			fputs("/>", out);
			break;
		default:
			break;
		}

		/* Second switch to handle the in_list state. */
		switch (node->style) {
			case RSTYLE_LIST_UNORDER:
				in_list = 1;
				break;
			case RSTYLE_LIST_ORDER:
				in_list = 2;
				break;
			default:
				if (in_list == 1)
					fputs("</ul>", out);
				else if (in_list == 2)
					fputs("</ol>", out);

				in_list = 0;
		}
	}

	if (NULL != cfg->html_footer)
		util_output_included(out, cfg->html_footer);

	fputs("</body>", out);
}

static void
_html_output_children(FILE *out, struct inline_node *children, int include_nl)
{
	struct inline_node 	*child;

	for (child = children; NULL != child; child = child->next) {
		switch (child->style) {
		case ISTYLE_NORMAL:
			fprintf(out, "%s", child->text);
			break;
		case ISTYLE_EM:
			fprintf(out, "<em>%s</em>", child->text);
			break;
		case ISTYLE_STRONG:
			fprintf(out, "<strong>%s</strong>", child->text);
			break;
		case ISTYLE_UNDERLINE:
			fprintf(out, "<u>%s</u>", child->text);
			break;
		default:
			break;
		}

		if (include_nl)
			fputc('\n', out);
	}
}

static void
_html_emit_tag(FILE *out, const char *tag, struct inline_node *children)
{
	int					 is_pre;

	fprintf(out, "<%s>", tag);

	/* We must include newlines if we're in a pre tag. */
	is_pre = strncmp("pre", tag, 3) == 0;

	_html_output_children(out, children, is_pre);

	fprintf(out, "</%s>", tag);
}
