/*
 * This file is part of Aix.
 * 
 * Aix is free software: you can redistribute it and/or modify
 * it under the terms of the Affero GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Aix is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Affero GNU General Public License for more details.
 * 
 * You should have received a copy of the Affero GNU General Public License
 * along with Aix.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef AIX_PARSER_H
#define AIX_PARSER_H

#include <stdio.h>

enum inline_style {
	ISTYLE_UNKNOWN		=  0,
	ISTYLE_UNDERLINE	= 'U',
	ISTYLE_NORMAL		= 'N',
	ISTYLE_STRONG		= 'S',
	ISTYLE_EM			= 'E'
};

struct inline_node {
	char				*text;
	enum inline_style	 style;
	struct inline_node	*next;
};

enum root_style {
	RSTYLE_UNKNOWN		=  0,
	RSTYLE_HEADER		= 'H',
	RSTYLE_SUBHEADER	= 'S',
	RSTYLE_PARAGRAPH	= 'P',
	RSTYLE_LIST_UNORDER	= 'U',
	RSTYLE_LIST_ORDER	= 'O',
	RSTYLE_QUOTE		= 'Q',
	RSTYLE_LINK			= 'L',
	RSTYLE_IMAGE		= 'I',
	RSTYLE_PRE			= 'C',
	RSTYLE_END			= 'E'
};

struct root_node {
	char				*meta;
	enum root_style		 style;
	struct inline_node	*children;
	struct root_node	*next;
};

struct root_node	*parser_read_file(FILE *);
void				 parser_free(struct root_node *);

#endif
